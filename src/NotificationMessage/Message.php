<?php

namespace Matok\NotificationMessage;


class Message implements MessageInterface
{
    private $title;
    private $content;

    public function __construct(string $title,string $content)
    {
        $this->title = $title;
        $this->content = $content;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setContent($content)
    {
        $this->content = $content;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function __toString()
    {
        return $this->title.'|'.$this->content;
    }
}