<?php
namespace Matok\NotificationMessage;

interface MessageInterface
{
    public function setTitle(string $title);
    public function getTitle() : string;

    public function setContent(string $content);
    public function getContent() : string;

    public function __toString();
}