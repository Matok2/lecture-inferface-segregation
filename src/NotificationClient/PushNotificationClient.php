<?php

namespace Matok\NotificationClient;

use Matok\NotificationMessage\MessageInterface;

class PushNotificationClient implements NotificationInterface
{
    private $endpointUrl;
    private $secret;

    public function __construct(string $endpointUrl, string $secret)
    {
        echo "PushNotificationClient::construct: $endpointUrl, $secret"."\n\n";

        $this->endpointUrl = $endpointUrl;
        $this->secret = $secret;
    }

    public function send(MessageInterface $message)
    {
        echo "PushNotificationClient::send: {$message->getTitle()}"."\n\n";

        $targetParams = array('title' => $message->getTitle(), 'content' => $message->getContent(), 'sign' => $this->sign($message));
        $targetUrl = $this->prepareTargetUrl($targetParams);

        echo "Sending push notification to: $targetUrl"."\n";
        echo "SUCCESS"."\n\n";
    }

    public function sign(MessageInterface $message): string
    {
        echo "PushNotificationClient::sign: $message"."\n\n";

        return sha1($this->secret.'|'.$message);
    }

    private function prepareTargetUrl($params)
    {
        array_walk($params, function(&$value, $key) {
            $value = $key.'='.urlencode($value);
        });
        return $this->endpointUrl.'?'.implode('&', $params);
    }
}