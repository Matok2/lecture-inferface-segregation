<?php

namespace Matok\NotificationClient;

use Matok\NotificationMessage\MessageInterface;

interface NotificationInterface
{
    public function send(MessageInterface $message);

    public function sign(MessageInterface $message): string;
}