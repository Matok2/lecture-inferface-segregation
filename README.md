# Lecture: Interface Segregation Principle

## Install
```
composer install
```

## Run
```
php app.php
```

## Tasks

1. alter PushNotificationClient to implement CryptoInterface
2. CryptoInterface has two methods but we need only one - split CryptoInterface into EndoceInfercate and DecodeInterface
3. alter PushNotificationClient to implement EncodeInterface only