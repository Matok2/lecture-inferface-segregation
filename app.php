<?php
include './vendor/autoload.php';

use Matok\NotificationClient\PushNotificationClient;
use Matok\NotificationMessage\Message;

$pushNotificationClient = new PushNotificationClient('https://example.com/push/send', 'not-a-secret');

$message = new Message('ISP', 'ISP Lecture');
$pushNotificationClient->send($message);

