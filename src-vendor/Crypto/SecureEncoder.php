<?php

namespace Matok\Crypto;


class SecureEncoder implements EncodeInterface
{
    public function encode(string $string): string
    {
        return sha1($string);
    }
}