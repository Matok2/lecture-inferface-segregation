<?php
namespace Matok\Crypto;

interface CryptoInterface
{
    public function decode(string $string): string;

    public function encode(string $string): string;
}